from flask import Flask
from flask import render_template
from flask import send_file

app = Flask(__name__, static_url_path="/static", static_folder="/static")
app.static_folder = 'static'

@app.route("/")
def index():
    return render_template("main/index.html")

@app.route("/MathLog")
@app.route("/MathLog/<path>")
def math_log(path = None):
    return render_template("MathLog/main.html")

@app.route("/uploads/resume")
def download_resume():
    return send_file("static/main/documents/resume.pdf")