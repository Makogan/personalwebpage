MathLog_path = "/static/MathLog/pages/";

document.addEventListener('readystatechange', event => {
  if (event.target.readyState === "complete") {
    changeSection("#VolatileSection", path, "home_button");
  }
});

function load() {
  var path = window.location.pathname;

  if(path == '/MathLog') {
    setHome();
  }

  else if(path == '/MathLog/lagrange') {
    setLagrange();
  }
};

document.onload = load();

function setHome() {
  path = MathLog_path + "home.html";
  changeSection("#VolatileSection", path, "home_button");
}

function setModelling() {
  clearButtons();
  setButton('modelling_button');
  expandNavList('modelling_sublist');
}
var points = [[0.6,0.2], [-0.2,0.2], [-0.2,-0.6]];
function setLagrange() {
  path = MathLog_path + "modelling/lagrange_interpolation.html";

  changeSection("#VolatileSection", path, "lagrange_button", false);

  $('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
  });

  var pointCanvas = createGlCanvas('#pointsCanvas');
  clear(pointCanvas.context);
  render(pointCanvas, [].concat(...points), pointCanvas.context.POINTS);

  var linearCanvas = createGlCanvas('#glCanvas1');
  clear(linearCanvas.context);

  render(linearCanvas, [].concat(...points), linearCanvas.context.POINTS);
  render(linearCanvas, [].concat(...points), linearCanvas.context.LINE_STRIP);

  var splineCanvas = createGlCanvas('#glCanvas2');
  clear(splineCanvas.context);

  var curve = [];
  var t = 0;
  var knots = [];
  for(var i=0; i<points.length; i++) {
    knots.push(i);
  }

  while(t <= knots[knots.length-1]+0.01) {
    var cPoint = lagrange(knots,points,t);
    curve.push([cPoint[0],cPoint[1]]);
    t+=0.1;
  }

  render(splineCanvas, [].concat(...points), splineCanvas.context.POINTS);
  render(splineCanvas, [].concat(...curve), splineCanvas.context.LINE_STRIP);

  drawLagrange();
  var slider = document.getElementById('tSlider');
  slider.oninput = function () { drawLagrange(this.value/100.0); }

}

function drawLagrange(tVal=0.25) {
  var knot1 = document.getElementById('knot1').value;
  var knot2 = document.getElementById('knot2').value;
  var knot3 = document.getElementById('knot3').value;

  knot1 = parseFloat(knot1);
  knot2 = parseFloat(knot2);
  knot3 = parseFloat(knot3);

  var knots = [knot1, knot2, knot3];

  tVal = tVal*knots[knots.length-1];
  var algoVisCanvas = createGlCanvas('#algoVisCanvas');
  clear(algoVisCanvas.context);
  var t=knots[0];
  var curve = [];
  while(t <= knots[knots.length-1]) {
    var cPoint = lagrange(knots,points,t);
    curve.push([cPoint[0],cPoint[1]]);
    t+=0.1;
  }
  curve.push(arrayClone(points[points.length-1]));
  var algoPyramid = lagrangeStage(knots, points, tVal);

  var colors = [[0.321, 0.596, 1,1],
                [0.372, 1, 0.321,1],
                [1, 0.321, 0.337,1]
               ];
  //render(algoVisCanvas,  [].concat(...[[0,0], [0,0]]));
  for (var i=0; i < points.length-1; i++) {
    setColor(algoVisCanvas, colors[i]);

    for(var j=0; j<algoPyramid[i].length-1; j++) {
      var p1 = arrayClone(algoPyramid[i][j]);
      var p2 = arrayClone(algoPyramid[i][j+1]);
      p1[0] = p1[0] - (p2[0]-p1[0])*500;
      p1[1] = p1[1] - (p2[1]-p1[1])*500;
      p2[0] = p2[0] - (p1[0]-p2[0])*500;
      p2[1] = p2[1] - (p1[1]-p2[1])*500;
      render(algoVisCanvas,  [].concat(...[p1, p2]));
    }
  }
  setColor(algoVisCanvas, [0,0,0,1]);
  render(algoVisCanvas, [].concat(...curve), algoVisCanvas.context.LINE_STRIP);

  for (var i=0; i< points.length; i++) {
    setColor(algoVisCanvas, colors[i]);
    render(algoVisCanvas, [].concat(...algoPyramid[i]), algoVisCanvas.context.POINTS);
  }
}

function setSpline() {
  path = MathLog_path + "modelling/spline.html";
  changeSection("#VolatileSection", path, "spline_button");
}

function expandNavList(sublistID) {
  var list = document.getElementById(sublistID);
  if (list.style.maxHeight){
    list.style.maxHeight = null;
  } else {
    list.style.maxHeight = list.scrollHeight + "px";
  }
}

function changeSection(section, page, button, sync=true)
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      $(section).empty();
      $(section).append(this.responseText);

      clearButtons();
      setButton(button);

        MathJax.Hub.Config({
    tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
  });
  MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
    }
  };
  xhttp.open("GET", page, sync);
  xhttp.send();
}

function fetchFile(file) {
  var file_contents;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      file_contents = this.responseText;
    }
  };
  xhttp.open("GET", file, false);
  xhttp.send();

  return file_contents;
}

function setButton(id) {
  var button = document.getElementById(id);
  button.classList.remove('NavButton');
  button.classList.add('NavButtonS');
}

function clearButtons()
{
  var x = document.getElementsByTagName("button");
  var i;
  for (i = 0; i < x.length; i++) {
      x[i].classList.remove("NavButtonS");
      x[i].classList.add("NavButton");
  }
}