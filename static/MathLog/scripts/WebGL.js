function compileFromShaders(gl, vertex, fragment) {
    const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vertex);
    const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fragment);
  
    const shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);
  
    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
        return null;
    }
  
    return shaderProgram;
}
  
function loadShader(gl, type, source) {
    const shader = gl.createShader(type);

    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
        return null;
    }

    return shader;
}

var draw_type_loc;
var dims_loc;
var point_size_loc;
function createGlCanvas(canvasId) {
    const canvas = document.querySelector(canvasId);
    const gl = canvas.getContext("webgl");
    void gl.lineWidth(3);

    // Only continue if WebGL is available and working
    if (gl === null) {
        alert("Unable to initialize WebGL. Your browser or machine may not support it.");
        return;
    }

    const webgl_path = "/static/MathLog/WebGL/";
    var vertex_shader = fetchFile(webgl_path + "vertex.glsl");
    var fragment_shader = fetchFile(webgl_path + "fragment.glsl");
    const basic_shader = compileFromShaders(gl, vertex_shader, fragment_shader);

    const programInfo = {
        program: basic_shader,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(basic_shader, 'aVertexPosition'),
        },
        uniformLocations: {
            projectionMatrix: gl.getUniformLocation(basic_shader, 'uProjectionMatrix'),
            modelViewMatrix: gl.getUniformLocation(basic_shader, 'uModelViewMatrix'),
        },
    };

    gbuffers = initBuffers(gl);

    const fieldOfView = 45 * Math.PI / 180;   // in radians
    const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 100.0;
    const projectionMatrix = mat4.create();

    /*mat4.perspective(projectionMatrix,
    fieldOfView,
    aspect,
    zNear,
    zFar);*/

    const modelViewMatrix = mat4.create();

    /*mat4.translate(modelViewMatrix,     // destination matrix
      modelViewMatrix,     // matrix to translate
      [-0.0, 0.0, -0.0]);  // amount to translate*/

    const numComponents = 2;  // pull out 2 values per iteration
    const type = gl.FLOAT;    // the data in the buffer is 32bit floats
    const normalize = false;  // don't normalize
    const stride = 0;         // how many bytes to get from one set of values to the next
                              // 0 = use type and numComponents above
    const offset = 0;         // how many bytes inside the buffer to start from

    gl.vertexAttribPointer(
        programInfo.attribLocations.vertexPosition,
        numComponents,
        type,
        normalize,
        stride,
        offset);
    gl.enableVertexAttribArray(
        programInfo.attribLocations.vertexPosition);

    gl.useProgram(programInfo.program);

    gl.uniformMatrix4fv(
        programInfo.uniformLocations.projectionMatrix,
        false,
        projectionMatrix);
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.modelViewMatrix,
        false,
        modelViewMatrix);

    draw_type_loc = gl.getUniformLocation(programInfo.program, 'draw_type');
    dims_loc = gl.getUniformLocation(programInfo.program, 'dims');
    point_size_loc = gl.getUniformLocation(programInfo.program, 'point_size');
    var color_loc = gl.getUniformLocation(programInfo.program, 'color');

    var width = canvas.offsetWidth;
    var height = canvas.offsetHeight;
    gl.uniform2f(dims_loc, width, height);
    gl.uniform1f(point_size_loc, 0.0001);
    gl.uniform4f(color_loc, 0.0,0.0,0.0,1.0);

    return{
        buffers: gbuffers,
        context: gl,
        color_loc: color_loc,
    };
}

function setPointSize(gl, size) {
    gl.uniform1f(point_size_loc, size);
}

function clear(gl) {
    // Set clear color to black, fully opaque
    gl.clearColor(1, 1, 1, 1.0);
    // Clear the color buffer with specified clear color
    gl.clear(gl.COLOR_BUFFER_BIT);
}

function setColor(canvas, color) {
    gl = canvas.context;
    gl.uniform4f(canvas.color_loc, color[0],color[1],color[2],color[3]);
}

function render(canvas, vertices, mode=canvas.context.LINE_STRIP){
    gl = canvas.context;
    buffer = canvas.buffers.position;
    gl.enable(gl.BLEND);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

    if(mode === gl.POINTS) {
        gl.uniform1i(draw_type_loc, 0);
    } else if(mode === gl.LINE_STRIP) {
        gl.uniform1i(draw_type_loc, 1);
    }

    setVertexBuffer(gl, vertices, buffer);
    // Set the shader uniforms
    {
        const offset = 0;
        const vertexCount = vertices.length/2;

        gl.drawArrays(mode, offset, vertexCount);
    }
}

function setVertexBuffer(gl, vertices, buffer) {
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER,
        new Float32Array(vertices),
        gl.STATIC_DRAW);
}

function initBuffers(gl) {
    const positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    return {
      position: positionBuffer,
    };
}