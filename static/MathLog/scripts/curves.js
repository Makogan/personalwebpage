function arrayClone( arr ) {
    var i, copy;

    if( Array.isArray( arr ) ) {
        copy = arr.slice( 0 );
        for( i = 0; i < copy.length; i++ ) {
            copy[ i ] = arrayClone( copy[ i ] );
        }
        return copy;
    } else if( typeof arr === 'object' ) {
        throw 'Cannot clone array containing an object!';
    } else {
        return arr;
    }

}

function lagrangeStage(knots, points, t) {
    // List lengths must match
    var point_num = points.length;
    if(knots.length===points.length) {
        point_num = points.length;
    }
    else {
        alert('Lagrange function was given knots and points with un matching lengths: '
            + points.length + ', ' + knots.length);
        return;
    }
    // Initialize list of curve points
    var control_points = arrayClone(points);
    var pyramid = [];
    pyramid.push(arrayClone(control_points));
    // Iterate from lowest polynomial order to highest
    for (var delta=1; delta<point_num; delta++) {
        // Generate the points of order d from the
        // points of order d-1
        pyramid.push([]);
        for(var ti=0; ti<point_num-delta; ti++) {
            // Calculate current weight for the
            // left side point
            var poly_val1 = ((knots[ti+delta]-t)/(knots[ti+delta]-knots[ti]));
            // Calculate current weight for the
            // right side point
            var poly_val2 = ((t-knots[ti])/(knots[ti+delta]-knots[ti]));
            // Calculate new point of order d
            // from the 2 points of order d-1
            var x = poly_val1*control_points[ti][0] + poly_val2*control_points[ti+1][0];
            var y = poly_val1*control_points[ti][1] + poly_val2*control_points[ti+1][1];
            control_points[ti][0] = x;
            control_points[ti][1] = y;

            pyramid[delta].push(arrayClone(control_points[ti]));
        }
    }
    return pyramid;
}

function lagrangePolys(knots, t) {
    // List lengths must match
    var point_num = knots.length;
    // Initialize list of curve points
    var control_points = [];
    for(var i=0; i<point_num; i++) {
        control_points.push(1);
    }
    var poly_val1 = 1;
    var poly_val2 = 1;
    // Iterate from lowest polynomial order to highest
    for (var delta=1; delta<point_num; delta++) {
        // Generate the points of order d from the
        // points of order d-1
        for(var ti=0; ti<point_num-delta; ti++) {
            // Calculate current weight for the
            // left side point
            poly_val1 = ((knots[ti+delta]-t)/(knots[ti+delta]-knots[ti]));
            // Calculate current weight for the
            // right side point
            poly_val2 = ((t-knots[ti])/(knots[ti+delta]-knots[ti]));
            // Calculate new point of order d
            // from the 2 points of order d-1
            var y = poly_val1*control_points[ti] + poly_val2*control_points[ti+1];
            control_points[ti] = y;
        }
    }
    return [poly_val1, poly_val2]
}

function lagrange(knots, points, t) {
    // List lengths must match
    var point_num = points.length;
    if(knots.length===points.length) {
        point_num = points.length;
    }
    else {
        alert('Lagrange function was given knots and points with un matching lengths: '
            + points.length + ', ' + knots.length);
        return;
    }
    // Initialize list of curve points
    var control_points = arrayClone(points);
    // Iterate from lowest polynomial order to highest
    for (var delta=1; delta<point_num; delta++) {
        // Generate the points of order d from the
        // points of order d-1
        for(var ti=0; ti<point_num-delta; ti++) {
            // Calculate current weight for the
            // left side point
            var poly_val1 = ((knots[ti+delta]-t)/(knots[ti+delta]-knots[ti]));
            // Calculate current weight for the
            // right side point
            var poly_val2 = ((t-knots[ti])/(knots[ti+delta]-knots[ti]));
            // Calculate new point of order d
            // from the 2 points of order d-1
            var x = poly_val1*control_points[ti][0] + poly_val2*control_points[ti+1][0];
            var y = poly_val1*control_points[ti][1] + poly_val2*control_points[ti+1][1];
            control_points[ti][0] = x;
            control_points[ti][1] = y;
        }
    }

    return control_points[0]
}