<style>
    .MText {
        margin: 10%;
        text-align: justify;
    }

    .Math {
        text-align: center;
    }

    .disp {
        border: 1px solid rgba(107, 31, 31, 0.671);
        -webkit-box-shadow: 2px 2px 5px -2px #888;  /* Safari 3-4, iOS 4.0.2 - 4.2, Android 2.3+ */
        -moz-box-shadow:    2px 2px 5px -2px #888;  /* Firefox 3.5 - 3.6 */
        box-shadow:         2px 2px 5px -2px #888;
    }

    .note{
        font-style: italic;
        font-size: 10pt;
        padding: 3%;
        background-color: rgb(233, 233, 233);
        text-align: left;
    }
    /*div {
        border: solid 2px red;
    }*/
    .slider {
        -webkit-appearance: none;
        width: 46%;
        height: 30px;
        background: #d3d3d3;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
        margin: 0;
        padding: 0;
    }

    .slider:hover {
        opacity: 1;
    }

    .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 30px;
        height: 30px;
        background: rgba(77, 77, 77, 0.76);
        cursor: pointer;
    }

    .slider::-moz-range-thumb {
        width: 30px;
        height: 30px;
        background: rgba(77, 77, 77, 0.76);
        cursor: pointer;
    }

    #tSlider {
        margin-bottom: -11px;
    }

    .knots {
        width: 5%;
        text-align: right;
        padding: 0;
        margin: 0;
        margin-top: -5px;
    }
</style>
<div class="MText">
    <h1 id="toc_1">Lagrange Interpolation</h1>

    <p>
        Given a set of points, lagrange interpolation will find a polynomial that passes
        <b>through</b> them, both geometrically and analytically. In other words, it
        guarantees a parametric path containing each of the data points.
    </p>

    <hr>
    <h2>
        Short Reference
        <a href="#toc_2" style="font-size: 9px"> (Jump to full explanation)</a>
    </h2>

    <h3 id="toc_1.1.1">Mathematical definition</h3>

    <p>
        Let $C = \{c_0 \dots c_i \dots c_{n-1}\} \subset V$ be a subset of a vector space $V$ of $n$ control points,
        and $K \subset \mathbb{R}$ be a finite
        set of associated knot
        values. The degree of the polynomial associated to the set $C$ of points is
        $d=n-1$ and the polynomial itself can be calculated through the recursive formula:
    </p>

    <p class="Math">
        $p_{i}^{d}(t)=
        \begin{cases}
          1 &amp; \text{if }d = 0 \\
          \frac{t_{i+d}-t}{t_{i+d}-t_i}\cdot p_{i}^{d-1}(t) + \frac{t-t_i}{t_{i+d}-t_i}\cdot
          p_{i+1}^{d-1}(t), &amp; \text{if } d&gt;0
       \end{cases}$
    </p>

    When the polynomial is combined with the control points it becomes:

    <p class="Math">
        $p_{i}^{d}(t)=
        \begin{cases}
            c_i &amp; \text{if }d = 0 \\
            \frac{t_{i+d}-t}{t_{i+d}-t_i}\cdot p_{i}^{d-1}(t) + \frac{t-t_i}{t_{i+d}-t_i}\cdot
            p_{i+1}^{d-1}(t), &amp; \text{if } d&gt;0
        \end{cases}$
    </p>

    <p>Where $t_i \in K$ is a knot value. </p>

    <p>The parametric curve $r$ is evaluated as:</p>

    <p class="Math">
    $r(t)=
        \begin{cases}
          c_i &amp; \text{if }d = 0 \\
          \frac{t_{i+d}-t}{t_{i+d}-t_i}\cdot p_{i}^{d-1}(t) + \frac{t-t_i}{t_{i+d}-t_i}\cdot
          p_{i+1}^{d-1}(t), &amp; \text{if } d&gt;0
       \end{cases}$
    </p>

    <p>Where $c_i \in C$ is a control point.</p>

    <blockquote>
    <p class="note">These polynomials are called <b>Lagrange Polynomials</b>. For a more succint
        definition you can refer to
        <a href="http://mathworld.wolfram.com/LagrangeInterpolatingPolynomial.html">
            Wolfram Alpha
        </a>
    </p>
    </blockquote>

    <h3 id="toc_1.1.2">Algorithm</h3>

    <p>
        Although one could simply implement the recursion above, it is far more efficient
        to use a dynamic programming appproach to compute the parametric curve. The
        following python implementation shows how to calculate the parametric curve
        efficiently.
    </p>

    <pre><code class="python hljs disp">
    def p_curve(knots, points, t):
        # List lengths must match
        assert(len(knots)==len(points))
        point_num = len(points)

        # Initialize list of curve points
        control_points = points.copy()
        # Iterate from lowest polynomial order to highest
        for delta in range (1, point_num):
            # Generate the points of order d from the
            # points of order d-1
            for ti in range (0, point_num-delta):
                # Calculate current weight for the
                # left side point
                poly_val1 = ((knots[ti+delta]-t)/(knots[ti+delta]-knots[ti]))
                # Calculate current weight for the
                # right side point
                poly_val2 = ((t-knots[ti])/(knots[ti+delta]-knots[ti]))
                # Calculate new point of order d
                # from the 2 points of order d-1
                control_points[ti]=(poly_val1*control_points[ti]+poly_val2*control_points[ti+1])

        return control_points[0]
    </code></pre>

    <p>
        We can consider that the original control points are a list of polynomial
        interpolation points of order 0 (constant points). From those we calculate all
        polynomial interpolation points of order 1 (straight lines). From the points of
        order 1 we calculate those of order 2 (quadratic curves).
        We proceed like this until we reach the same order as the number of control
        points.
    </p>

    <p>
        Since the number of points is reduced by one with each level, the final computation
        creates a single point, which is the result of the evaluation of the interpolation
        curve at parameter $t$ based on the knot vector $K$ and control points matrix $C$.
     </p>

    <hr>

    <h2 id="toc_2">Intuition and explanation</h3>

        <p>
            A common problem that is found in mathematics is trying to generate a curve from
        a set of points.  A first way to understand this is as a data analyst.
        </p>
        <p>
           Imagine we have collected some data and plotted it in a graph. We want to try to complete the
        gaps between points using the information we already have, to generate
        a final, continuous function that explains our data. This process is called interpolation,
        as it completes data <i>within</i> the range of sample points (as opposed to
        extrapolation, which tries to predict what the data looks like outside the sample
        points).
        </p>
        <p>
            There is however a geometric way to see the same problem, which I find more
        intuitive.
        <br>
        Given a set of points in a space, we want to create a path that
        connects those dots.
        <br>
        So for example, imagine we have the following 3 points
        in the plane:
        </p>
        <div style="text-align: center; padding: 0; margin: 20px">
            <canvas id="pointsCanvas" class="disp" width=700 height=500></canvas>
        </div>
        <p>
            The simplest way to do this would be to just connect the points with
            straight lines.
        </p>

        <div style="text-align: center; padding: 0; margin: 20px">
            <canvas id="glCanvas1" class="disp" width=700 height=500></canvas>
        </div>

        <p>
            However, this is very limited, as we would always have a jaggy line and never
            a smooth, continuous path connecting our points.
        </p>
        <p>
            We often want a curved path like the one below:
        </p>
        <div style="text-align: center; padding: 0; margin: 20px">
            <canvas id="glCanvas2" class="disp" width=700 height=500></canvas>
        </div>
        <p>
            The way to generate a curve like the one displayed above can be described in a
            geometric fashion fairly simply.
            <br><br>
            Assume we start with our 3 points $p_0$, $p_1$, $p_2$ and associated weights
            [0,1,2]. Now say we want to create the point at parameter $t=0.5$. Then we
            would do the following process:
            <br><br>
            0.5 is the middle of [0,1] (weights 1 and 2) and it is -0.5 away from the
            interval [1,2] (weights 2 and 3), or
            more formally $0.5 = \frac{0.5-0}{1=0}$ and $-0.5=\frac{0.5-1}{2-1}$. So what
            we will do is take the straight line connecting $p_0$ and $p_1$ and move 0.5
            units from $p_0$ to $p_1$ along that line to create $p'_0$; then we will take
            the straight line connecting $p_1$ and $p_2$ move 0.5 units AWAY from $p_1$
            (or -0.5 towards $p_2$) and create $p'_1$.
            <br><br>
            0.5 is 0.25 of the interval [0,2] , or formally $0.25=\frac{0.5 - 0}{2-0}$. So
            now we will move 0.25 units from $p'_0$ towards $p'_1$ and create $p''_0$
            which is our final point.
            <br><br>
            More generally, using the knots and the parameter we create a new point for
            each pair of control points. We then take these new points and repeat the
            process until we get exactly one point.
            <br><br>
            Below you can find an interactive visualization of this process where you can
            see how the curve is generated from the straight segments and you can modify
            the weights to see how the curve changes.
        </p>

        <div style="text-align: center; padding: 0; margin: 20px">
            <canvas id="algoVisCanvas" class="disp" width=700 height=500></canvas>
        </div>

        <div style="font-size: 35px; text-align: center">
            <input type="text" id="knot1" value="0" class="knots">,
            <input type="text" id="knot2" value="1" class="knots">,
            <input type="text" id="knot3" value="2" class="knots">
            <input type="range" min="0" max="100" value="50" id="tSlider" class="slider">
        </div>
        <div class="note" style="margin-top: 18px; margin-bottom: 18px">
            Instructions: use the slider to change the parameter and see the generated
            point.
            <br>
            Modify the weights to see how they affect the curve (make sure they are
            strictly increasing).
        </div>
        <div>
            The geometric explanation may suffice for some people, but others prefer a more
            procedural way to see problems, so I will describe a different fashion to produce
            the curve.
            <br><br>
            Once again we have control points $c_0,c_1,c_2$, weights $[0,1,2]$ and parameter $t=0.5$.
            Noticing how the polynomials of degree $0$ are just the control points at any value of $t$
            allows us to generate
            the first level of our evaluation. The level 0 thus becomes $[c_0, c_1, c_2]$
            <br><br>
            The next level is made by linearly combining the previous level. Just as above
            $t=0.5$ has the relative distances $0.5$ and $-0.5$.<br> We thus build level 1 as:
            $\bigg[\frac{1-0.5}{1-0}c_0+\frac{0.5-0}{1-0}c_1, \frac{2-0.5}{2-1}c_1 + \frac{0.5-1}{2-1}c_2\bigg]=\bigg[p^1_0,p^1_1\bigg]$.
            <br><br>
            Once again the next level is made from linear combinations of the prior based on
            the value of $t$. Just as above the relative distance of $t$ at this level is (0.75),
            and  as such
            level 2 is:
            $\bigg[\frac{2-0.5}{2-0}p^1_0, \frac{0.5-1}{2-0}\bigg] = \bigg[p^2_0\bigg]$
            <br><br>
            As you can see, all we do is calculate where the parameter $t$ is relatively in a
            given interval; then we interpolate the 2 values of the current level based on
            that distance, to create a single point in the next level.
            <br><br>
            The more general diagram to do this for any arbitrary knot sequence with 3 values
            and 3 control points it outlined below:
            <div style="text-align: center">
                <img src="/static/MathLog/images/knot_lattice.png" width="500px" height="500px">
            </div>
        </div>
        <div>
            <p>
                From the above explanation we can generalize the process that was
                originally described at the top of this page, however some readers may be
                confused as the recursive definition does not match the most common
                definition of a Lagrange Polynomial.
                <br><br>
                Most resources will define a lagrange polynomial as:
                $$L(x)=\sum^{d}_{j=0}\prod_{\substack{0\leq m\leq d\\ j\neq m}}\frac{t-t_m}{t_j-t_m}$$
                Where each one of the products in the sum is one basis polynomials that
                compose the lagrange polynomial. For $n$ knots, each basis will be a
                polynomial of degree $n-1$ and there will be exactly $n$ such polynomials.
            </p>
            <div>
                <img src="/static/MathLog/images/LagrangeBasis.png" width="700px" height="470px">
                <div class="note">
                    Lagrange basis polynomials for the first three degrees, defined on a
                    normalized range from -1 to 1.
                </div>
            </div>
            <p>
                <br><br>
                Indeed, one could compute the same curve as above by calculating each basis
                polynomial, multiplying it by one of the control points and adding them
                together to create the final point. Which may be simpler to code. However,
                the DP algorithm will take $\frac{n^2+n}{2}$ steps to compute, whereas
                computing the basis directly and adding them will take $n^2$ steps, thus
                it is more efficient to do the recursive algorithm in general.
                <br>
                As such it is important to show that the 2 definitions are equivalent, and we
                will do so below:
            </p>
        </div>
        <div>
        But first we will define one symbol. The basis $k \in [i,j]$ of the knot sub-sequence $[t_i,t_{j}]$
        $$l^{i,j}_{k}(t) = \prod_{\substack{ i\leq m \leq j\\ k\neq m}}\frac{t-tm}{t_k-tm}$$

        Now for the proof itself. <br>

        We want to prove that for any degree $d$ and knot $i$ in the knot sequence:

        $$p_i^d(t) = \sum^{i+d}_{j=i}\prod_{\substack{i\leq m \leq i+d }}\frac{t-t_m}{t_j-t_{m}}
        =\sum^{i+d}_{j=i}l^{i,i+d}_j(t)$$

        <h3>Base case:</h3>

        \begin{equation}
        \begin{split}
        P^1_i(t) &= \frac{t_{i+1}-t}{t_{i+1}-t_i} + \frac{t-t_i}{t_{i+1}-t_i}\\
        &= \frac{t-t_{i+1}}{t_i-t_{i+1}} + \frac{t-t_i}{t_{i+1}-t_i} \\
        \end{split}
        \end{equation}

        Which is trivially

        $$\sum^{i+1}_{j=i}\prod_{\substack{i\leq m \leq i+1\\ j \neq m }}\frac{t-t_m}{t_j-t_m}$$

        <h3>Inductive case:<br><br></h3>

        Assume that for all $p_{i}^{d-1}$:

        $$p_i^{d-1}(t) = \sum^{i+d-1}_{j=i}\prod_{\substack{i\leq m \leq i+d-1\\ j\neq m }}\frac{t-t_m}{t_j-t_{m}}$$

        Then:
        \begin{equation*} 
        \begin{split}
        P^d_i(t) &= \frac{t_{i+d}-t}{t_{i+d}-t_i}\cdot p^{d-1}_i(t) + \frac{t-t_i}{t_{i+d}-t_i}\cdot p^{d-1}_{i+1}(t)\\[16pt]
        \end{split}
        \end{equation*}
        Since $\frac{t_{i+d}-t}{t_{i+d}-t_i} = \frac{-(t-t_{i+d})}{-(t_i- t_{i+d})}$
        \begin{equation*}
        \begin{split}
        P^d_i(t) &= \frac{t-t_{i+d}}{t_i-t_{i+d}}\cdot p^{d-1}_i(t) + \frac{t-t_i}{t_{i+d}-t_i}\cdot p^{d-1}_{i+1}(t)\\[16pt]
        %
        &= \Bigg(\frac{t-t_{i+d}}{t_i-t_{i+d}}\Bigg)\Bigg(\sum^{i+d-1}_{j=i}\prod_{\substack{i \leq m \leq d-1 \\ j \neq m}}\frac{t-t_m}{t_j-t_m
        }\Bigg)\\
        %
        &\hspace{3pt} +  \Bigg(\frac{t-t_{i}}{t_{i+d}-t_i}\Bigg)\Bigg(\sum^{i+d}_{j=i+1}\prod_{\substack{i+1 \leq m \leq d \\ j \neq m}}\frac{t-t_m}{t_j-t_m
        }\Bigg)\\[16pt]
        \end{split}
        \end{equation*}
        We can then extract the first term from the first sum and the last term from the second sum, giving:
        \begin{equation*}
        \begin{split}
        &= \Bigg(\frac{t-t_{i+d}}{t_i-t_{i+d}}\Bigg)\Bigg(\sum^{i+d-1}_{j=i+1}\prod_{\substack{i \leq m \leq d-1 \\ j \neq m}}\frac{t-t_m}{t_j-t_m
        }\Bigg) +  \Bigg(\frac{t-t_{i+d}}{t_i-t_{i+d}}\Bigg)\Bigg(\prod_{\substack{i \leq m \leq d-1 \\ i \neq m}}\frac{t-t_m}{t_i-t_m
        }\Bigg)\\
        %
        &\hspace{3pt} +  \Bigg(\frac{t-t_{i}}{t_{i+d}-t_i}\Bigg)\Bigg(\sum^{i+d-1}_{j=i+1}\prod_{\substack{i+1 \leq m \leq d \\ j \neq m}}\frac{t-t_m}{t_j-t_m
        }\Bigg)+ \Bigg(\frac{t-t_{i}}{t_{i+d}-t_i}\Bigg)\Bigg(\prod_{\substack{i+1 \leq m \leq d \\ i+d \neq m}}\frac{t-t_m}{t_{i+d}-t_m
        }\Bigg)\\[16pt]
        &=  \Bigg(\frac{t-t_{i+d}}{t_i-t_{i+d}}\Bigg)\Bigg(\sum^{i+d-1}_{j=i+1}\prod_{\substack{i \leq m \leq d-1 \\ j \neq m}}\frac{t-t_m}{t_j-t_m
        }\Bigg) +
        \Bigg(\prod_{\substack{i \leq m \leq d \\ i \neq m}}\frac{t-t_m}{t_i-t_m
        }\Bigg)\\
        %
        &\hspace{3pt} +
        \Bigg(\frac{t-t_{i}}{t_{i+d}-t_i}\Bigg)\Bigg(\sum^{i+d-1}_{j=i+1}\prod_{\substack{i+1 \leq m \leq d \\ j \neq m}}\frac{t-t_m}{t_j-t_m
        }\Bigg)+
        \Bigg(\prod_{\substack{i \leq m \leq d \\ i+d \neq m}}\frac{t-t_m}{t_{i+d}-t_m
        }\Bigg)\\[16pt]
        \end{split}
        \end{equation*} 
        We can replace the 2 outer products by their notation. We can also note that in the first sum, each product contains the factor $\frac{t-t_i}{t_j-t_i}$ and in the second sum each product contains $\frac{t-t_{i+d}}{t_j-t_{i+d}}$ so by developing and showing these terms we get:
        \begin{equation*} 
        \begin{split}
        &= l^{i,i+d}_i(t) + l^{i,i+d}_{i+d}(t)\\
        &\hspace{3pt} + \sum^{i+d-1}_{j=i+1}\Bigg(\prod_{\substack{i+1 \leq m \leq d-1 \\ j \neq m}}\frac{t-t_m}{t_j-t_m
        }\Bigg)\Bigg(\frac{t-t_{i}}{t_j-t_{i}}\Bigg)\Bigg(\frac{t-t_{i+d}}{t_i-t_{i+d}}\Bigg)\\
        %
        &\hspace{3pt} +
        \sum^{i+d-1}_{j=i+1}\Bigg(\prod_{\substack{i+1 \leq m \leq d-1 \\ j \neq m}}\frac{t-t_m}{t_j-t_m
        }\Bigg)\Bigg(\frac{t-t_{i+d}}{t_j-t_{i+d}}\Bigg)\Bigg(\frac{t-t_{i}}{t_{i+d}-t_i}\Bigg)\\
        \end{split}
        \end{equation*} 
        Both sums are over the same range and same products, differing only in the last 2 factors of each product. Thus we can merge the sums and factor out each repeated product to get.
        \begin{equation*} 
        \begin{split}
        &= l^{i,i+d}_i(t) + l^{i,i+d}_{i+d}(t) \\
        &\hspace{3pt} + 
        \sum^{i+d-1}_{j=i+1}l^{i+1,i+d-1}_{j}(t)\Bigg[\Bigg(\frac{t-t_{i}}{t_j-t_{i}}\Bigg)\Bigg(\frac{t-t_{i+d}}{t_i-t_{i+d}}\Bigg)+\Bigg(\frac{t-t_{i+d}}{t_j-t_{i+d}}\Bigg)\Bigg(\frac{t_{i}-t}{t_i-t_{i+d}}\Bigg)\Bigg]\\
        \end{split}
        \end{equation*}
        Now working the term inside of the square brackets:
        \begin{equation*}
        \begin{split}
        &\Bigg[\Bigg(\frac{t-t_{i}}{t_j-t_{i}}\Bigg)\Bigg(\frac{t-t_{i+d}}{t_i-t_{i+d}}\Bigg)+\Bigg(\frac{t-t_{i+d}}{t_j-t_{i+d}}\Bigg)\Bigg(\frac{t_{i}-t}{t_i-t_{i+d}}\Bigg)\Bigg]\\
        &=\Bigg[\Bigg(\frac{t_i-t_{i+d}}{t_i-t_{i+d}}\Bigg)\Bigg(\frac{t-t_i}{t_j-t_i}+\frac{t_i-t}{t_j-t_{i+d}}\Bigg)\Bigg]\\
        %
        &=\Bigg[\Bigg(\frac{t-t_{i+d}}{t_i-t_{i+d}}\Bigg)\Bigg(\frac{t-t_i}{t_j-t_i}\cdot\frac{t_j-t_{i+d}}{t_j-t_{i+d}}+\frac{t_i-t}{t_j-t_{i+d}}\cdot\frac{t_j-t_i}{t_j-t_{i}}\Bigg)\Bigg]\\
        &= \Bigg[\Bigg(\frac{t-t_{i+d}}{t_i-t_{i+d}}\Bigg)\Bigg(\frac{(t-t_i)(t_j-t_{i+d}) -(t-t_i)(t_j-t_{i})}{(t_j-t_i)(t_j-t_{i+d})}\Bigg)\Bigg]\\
        %
        &= \Bigg[\Bigg(\frac{t-t_{i+d}}{t_i-t_{i+d}}\Bigg)\Bigg(\frac{(t-t_i)[(t_j-t_{i+d}) -(t_j-t_{i})]}{(t_j-t_i)(t_j-t_{i+d})}\Bigg)\Bigg]\\
        %
        &= \Bigg[\Bigg(\frac{t-t_{i+d}}{t_i-t_{i+d}}\Bigg)\Bigg(\frac{(t-t_i)(t_i-t_{i+d})}{(t_j-t_i)(t_j-t_{i+d})}\Bigg)\Bigg]\\
        %
        &=
        \Bigg[\frac{(t-t_{i+d})(t-t_i)}{(t_j-t_{i+d})(t_j-t_i)}\Bigg]\\
        \end{split}
        \end{equation*}
        Going back to the larger expression
        \begin{equation*} 
        \begin{split}
        p^d_i&= l^{i,i+d}_i(t) + l^{i,i+d}_{i+d}(t) \\
        &\hspace{3pt} + 
        \sum^{i+d-1}_{j=i+1}l^{i+1,i+d-1}_{j}(t)\Bigg[\frac{(t-t_{i+d})(t-t_i)}{(t_j-t_{i+d})(t_j-t_i)}\Bigg]\\
        &= l^{i,i+d}_i(t) + l^{i,i+d}_{i+d}(t) 
        \hspace{3pt} + 
        \sum^{i+d-1}_{j=i+1}l^{i,i+d}_{j}(t)\\
        &= \sum^{i+d}_{j=i}l^{i,i+d}_{j}(t)
        \end{split}
        \end{equation*}
        As required

        </div>
    <hr>

    <h3 id="toc_1.2">References</h3>

    <p>
        <a href="https://www.uio.no/studier/emner/matnat/ifi/nedlagte-emner/INF-MAT5340/v07/undervisningsmateriale/kap1.pdf">
            [1] Explanation of Interpolating curves from the University of Oslo.
            Author: Michael S. Floater.<br>
        </a>
        <a href="http://mathworld.wolfram.com/LagrangeInterpolatingPolynomial.html">
            [2] Wolfram Alpha section on Lagrange Interpolating Polynomials<br>
        </a>
        <a href="https://en.wikipedia.org/wiki/Lagrange_polynomial">
            [3] Wikipedia page on Lagrange Interpolating Polynomials<br>
        </a>
    </p>
</div>

<div id="disqus_thread"></div>
<script>
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://camilotalero-com.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<script id="dsq-count-scr" src="//camilotalero-com.disqus.com/count.js" async></script>