precision mediump float;

uniform int draw_type;
uniform float point_size;
uniform vec2 dims;
uniform vec4 color;

varying vec4 position;

void main() {
      if(draw_type == 0) {
            float r = 0.0, delta = 0.1, alpha = 1.0;

            vec2 cxy = 2.0 * gl_PointCoord - 1.0;
            r = dot(cxy, cxy);

            if(r > point_size){
                  alpha = 1.0-smoothstep(0.7,1.0, (sqrt(r) - sqrt(point_size))/0.1);
            }

            gl_FragColor = color*alpha;
      }

      else {
            gl_FragColor = color;
      }
}