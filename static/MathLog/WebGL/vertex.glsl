attribute vec4 aVertexPosition;

uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;

varying vec4 position;

void main() {
    gl_PointSize = 100.0;
    position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
    gl_Position = position;
}